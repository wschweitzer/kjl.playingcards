
// Playing Cards
// Keane Laux

#include <iostream>
#include <conio.h>
#include <ostream>

using namespace std;

enum Rank{ TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE };

enum Suit{ SPADES, DIAMONDS, CLUBS, HEARTS };

struct Card
{
	Rank rank;
	Suit suit;
};


void PrintCard(Card card)
{
	switch (card.rank)
	{
	case 2: cout << "The Two of ";
		break;
	case 3: cout << "The Three of ";
		break;
	case 4: cout << "The Four of ";
		break;
	case 5: cout << "The Five of ";
		break;
	case 6: cout << "The Six of ";
		break;
	case 7: cout << "The Seven of ";
		break;
	case 8: cout << "The Eight of ";
		break;
	case 9: cout << "The Nine of ";
		break;
	case 10: cout << "The Ten of ";
		break;
	case 11: cout << "The Jack of ";
		break;
	case 12: cout << "The Queen of ";
		break;
	case 13: cout << "The King of ";
		break;
	case 14: cout << "The Ace of ";
		break;
	}

	switch (card.suit)
	{
	case 0: cout << "Spades";
		break;
	case 1: cout << "Diamonds";
		break;
	case 2: cout << "Clubs";
		break;
	case 3: cout << "Hearts";
		break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank >= card2.rank)
	{
		return card1;
	}
	else
	{
		return card2;
	}
}


int main()
{
	Card card1;
	card1.rank = ACE;
	card1.suit = HEARTS;
	
	Card card2;
	card2.rank = QUEEN;
	card2.suit = SPADES;

	PrintCard(card1);
	HighCard(card1, card2);

	(void)_getch();
	return 0;
}

